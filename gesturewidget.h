#ifndef GESTUREWIDGET_H
#define GESTUREWIDGET_H

#include <QWidget>
#include "gestureview.h"
#include "gesturelabel.h"

class GestureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GestureWidget(QWidget* parent = 0);
    ~GestureWidget();
public slots:
private:
    GestureView* view;
    GestureLabel* label;

    bool event(QEvent* event);
};

#endif // GESTUREWIDGET_H
