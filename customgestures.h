#ifndef FOURFINGERTAP_H
#define FOURFINGERTAP_H

#include <QGesture>
#include <QGestureRecognizer>
#include <QVector>
#include <QPoint>

//class LGesture : public QGesture
//{
//    Q_PROPERTY(QString name
//               READ getName
//               WRITE setName)
//    Q_PROPERTY(QPointF pStart
//               READ getPStart
//               WRITE setPStart)
//    Q_PROPERTY(QPointF pEnd
//               READ getPEnd
//               WRITE setPEnd)
//public:
//    LGesture(QObject *parent = 0);

//    QString getName();
//    void setName(QString name);

//    QPointF getPStart();
//    void setPStart(QPointF pStart);

//    QPointF getPEnd();
//    void setPEnd(QPointF pEnd);
//private:
//    QString name;

//    QPointF pStart;
//    QPointF pEnd;
//};

class LRecognizer : public QGestureRecognizer
{
public:
    LRecognizer();
private:
    QPointF pStart;
    QPointF pEnd;
//    QPointF pNorm;
//    QPointF pDist;

    QVector<QPointF> positions;

    bool lineApproximation();

//    virtual QGesture *create(QObject *target);
    virtual Result recognize(QGesture *state, QObject *watched, QEvent *event);

};

/*********************************************************************************/

//class FFTGesture : public QGesture
//{
//    Q_PROPERTY(QString name
//               READ getName
//               WRITE setName)
//public:
//    FFTGesture(QObject *parent = 0);
//    QString getName();
//    void setName(QString name);
//private:
//    QString name;
//};

class FFTRecognizer : public QGestureRecognizer
{
public:
    FFTRecognizer();
private:
//    virtual QGesture *create(QObject *target);
    virtual Result recognize(QGesture *state, QObject *watched, QEvent *event);

};

#endif // FOURFINGERTAP_H
