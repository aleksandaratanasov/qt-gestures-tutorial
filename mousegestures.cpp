#include <QDebug>
#include <QtWidgets>
#include <QtMath>
#include "mousegestures.h"

// FIXME Calulcating the angle between two vectors seems to return a value only between 0 and 180 degrees.
//       Due to the implementation of swipe this issue doesn't seem to affect the gesture (though if fixed
//       it might be possible to simplify the code) however it greatly affects the pinch gesture. Since only
//       the interval mentioned above is covered the rotation is only partial and looks awkward

MousePinchGesture::MousePinchGesture(QObject* parent)
    : QGesture(parent),
      stretch(0.0),
      direction(PinchDirection::Undefined)
{

}

QPointF MousePinchGesture::getFirstLeft() const
{
    return this->firstLeft;
}

void MousePinchGesture::setFirstLeft(const QPointF& firstPinchPoint)
{
    this->firstLeft = firstPinchPoint;
}

QPointF MousePinchGesture::getSecondRight() const
{
    return this->secondRight;
}

void MousePinchGesture::setSecondRight(const QPointF& secondPinchPoint)
{
    this->secondRight = secondPinchPoint;
}

double MousePinchGesture::getStretch() const
{
    return this->stretch;
}

void MousePinchGesture::setStretch(double stretch)
{
    this->stretch = stretch;
}

MousePinchGesture::PinchDirection MousePinchGesture::getDirection() const
{
    return this->direction;
}

void MousePinchGesture::setDirection(MousePinchGesture::PinchDirection direction)
{
    this->direction = direction;
}

double MousePinchGesture::getScale() const
{
    return this->scale;
}

void MousePinchGesture::setScale(double factor)
{
    this->scale = factor;
}

double MousePinchGesture::getAngle() const
{
    return this->angle;
}

void MousePinchGesture::setAngle(double angle)
{
    this->angle = angle;
}


MousePinchRecognizer::MousePinchRecognizer()
    : QGestureRecognizer(),
      directionTemp(MousePinchGesture::PinchDirection::Undefined),
      stretch(0.0),
      scaleTemp(0.0),
      angleTemp(0.0)
{
    this->pinchStates[LeftButtonPressed] = false;
    this->pinchStates[LeftButtonReleased] = false;
    this->pinchStates[RightButtonPressed] = false;
}

QGestureRecognizer::Result MousePinchRecognizer::recognize(QGesture* state, QObject* watched, QEvent* event)
{
/*    if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->buttons() & Qt::LeftButton) {
            if (this->pinchStates[LeftButtonReleased] || this->pinchStates[RightButtonPressed]) {
                return ResultFlag::CancelGesture;
            }

            this->pinchStates[LeftButtonPressed] = true;
            return ResultFlag::MayBeGesture;
        }
        // If right button has been pressed
        else if (mouseEvent->button() & Qt::RightButton) {
            if (this->pinchStates[LeftButtonReleased] && !this->pinchStates[LeftButtonPressed]) {
                this->pinchStates[LeftButtonReleased] = false;
                this->pinchStates[RightButtonPressed] = true;

                this->

                processData();

                // Populate gesture!

                return ResultFlag::FinishGesture;
            }
            else {
                return ResultFlag::CancelGesture;
            }
        }
    }
    else if (event->type() == QEvent::MouseButtonRelease) {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->buttons() & Qt::LeftButton) {
            if (!this->pinchStates[LeftButtonPressed] || this->pinchStates[RightButtonPressed]) {
              return ResultFlag::CancelGesture;
            }
        }
    }*/

    if (event->type() == QEvent::MouseMove || event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonRelease) {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);

        if (mouseEvent->button() == Qt::LeftButton) {
            if (event->type() == QEvent::MouseButtonPress) {
//                qDebug() << "Entering state between left button pressed and released";
                this->pinchStates[LeftButtonPressed] = true;
                this->pinchStates[LeftButtonReleased] = false;
                this->pinchStates[RightButtonPressed] = false;
                return ResultFlag::MayBeGesture;
            }

            if ((event->type() == QEvent::MouseButtonRelease) && this->pinchStates[LeftButtonPressed]) {
//                qDebug() << "Entering state between left button released and right button pressed";
                this->pinchStates[LeftButtonPressed] = false;
                this->pinchStates[LeftButtonReleased] = true;
                this->pinchStates[RightButtonPressed] = false;

                this->firstLeft = mouseEvent->localPos();
                return ResultFlag::MayBeGesture;
            }
        }
        else if (mouseEvent->button() == Qt::RightButton) {
            if ((event->type() == QEvent::MouseButtonPress) && this->pinchStates[LeftButtonReleased]) {
//                qDebug() << "Entering state right button pressed";
                this->pinchStates[LeftButtonPressed] = false;
                this->pinchStates[LeftButtonReleased] = false;
                this->pinchStates[RightButtonPressed] = true;

                QVector2D origin = QVector2D(this->firstLeft);
                QVector2D secondRightInitial = QVector2D(mouseEvent->localPos());

                this->xAxis = secondRightInitial - origin;
                this->stretch = this->xAxis.length();

                if (this->stretch == 0.0) {
                    return ResultFlag::CancelGesture;
                }

                return ResultFlag::TriggerGesture;
            }

            if ((event->type() == QEvent::MouseButtonRelease) && this->pinchStates[RightButtonPressed]) {
//                qDebug() << "Exiting pinch";
                return ResultFlag::FinishGesture;
            }

        }
        else if (this->pinchStates[RightButtonPressed] && (event->type() == QEvent::MouseMove)) {
//            qDebug() << "Updating pinch";
            this->secondRight = mouseEvent->localPos();

            processData();

            state->setProperty("firstLeft", this->firstLeft);
            state->setProperty("secondRight", this->secondRight);
            state->setProperty("stretch", this->stretch);
            state->setProperty("scale", this->scaleTemp);
            state->setProperty("angle", this->angleTemp);
            state->setProperty("direction", this->directionTemp);

            return ResultFlag::TriggerGesture;
        }
    }

    return ResultFlag::Ignore;
}

QGesture*MousePinchRecognizer::create(QObject* target)
{
    MousePinchGesture* pinch = new MousePinchGesture(target); // target as parent or nullptr?!?
    return static_cast<QGesture*>(pinch);
}

void MousePinchRecognizer::reset(QGesture* state)
{
    for (int i = 0; i < 3; ++i) {
        this->pinchStates[i] = false;
    }

    this->firstLeft = QPointF();
    this->secondRight = QPointF();
    this->directionTemp = MousePinchGesture::PinchDirection::Undefined;
    this->xAxis.setX(0);
    this->xAxis.setY(0);
    this->currentSecondRightToOrigin.setX(0);
    this->currentSecondRightToOrigin.setY(0);
    this->previousSecondRightToOrigin.setX(0);
    this->previousSecondRightToOrigin.setY(0);
    this->scaleTemp = 0;
    this->stretch = 0;
    this->angleTemp = 0;

    MousePinchGesture* pinch = static_cast<MousePinchGesture*>(state);
    if (pinch) {
        pinch->setFirstLeft(QPointF());
        pinch->setSecondRight(QPointF());
        pinch->setAngle(0.0);
        pinch->setScale(0.0);
        pinch->setStretch(0.0);
        pinch->setDirection();
    }
}

void MousePinchRecognizer::processData()
{
    QVector2D secondRightCurrent = QVector2D(this->secondRight);
    this->currentSecondRightToOrigin = secondRightCurrent - QVector2D(this->firstLeft);

    this->scaleTemp = this->stretch + this->currentSecondRightToOrigin.length();

    // and use it to calculate the angle relative to the set X axis
    double cosSecondRightCurrentOrigin = (QVector2D::dotProduct(xAxis, this->currentSecondRightToOrigin))/(xAxis.length()*this->currentSecondRightToOrigin.length());
    this->angleTemp = qRadiansToDegrees(qAcos(cosSecondRightCurrentOrigin));

//    if (secondRightCurrent.y() > this->firstLeft) {
//        qDebug() << this->angleTemp+180.0;
//    }

    if (this->currentSecondRightToOrigin.length() < this->previousSecondRightToOrigin.length()) {
        this->directionTemp = MousePinchGesture::PinchDirection::Inward;
    }
    else if (this->currentSecondRightToOrigin.length() > this->previousSecondRightToOrigin.length()) {
        this->directionTemp = MousePinchGesture::PinchDirection::Outward;
    }
    else {
        this->directionTemp = MousePinchGesture::PinchDirection::Unchanged;
    }

    this->previousSecondRightToOrigin = this->currentSecondRightToOrigin;
}


MouseSwipeGesture::MouseSwipeGesture (QObject* parent)
    : QGesture(parent),
      direction(Undefined)
{
    setGestureCancelPolicy(QGesture::GestureCancelPolicy::CancelNone); // If gesture is accepted don't cancel other gestures
}

QPointF MouseSwipeGesture::getStart() const
{
    return this->start;
}

void MouseSwipeGesture::setStart(const QPointF& start)
{
    this->start = start;
}

QPointF MouseSwipeGesture::getEnd() const
{
    return this->end;
}

void MouseSwipeGesture::setEnd(const QPointF& end)
{
    this->end = end;
}

MouseSwipeGesture::SwipeDirection MouseSwipeGesture::getDirection() const
{
    return this->direction;
}

void MouseSwipeGesture::setDirection(MouseSwipeGesture::SwipeDirection direction)
{
    this->direction = direction;
}

MouseSwipeRecognizer::MouseSwipeRecognizer(int swipeDetectionTimeoutInMs)
    : QGestureRecognizer(),
      swipeTimeout(swipeDetectionTimeoutInMs),
      swipeDetected(false),
      activateSampling(false),
      directionTemp(MouseSwipeGesture::SwipeDirection::Undefined)
{
    if (!this->swipeTimeout) {
        qDebug() << "Swipe detection appears to be 0ms. Setting to default of 350ms";
    }
}

void MouseSwipeRecognizer::setDetectionTimeout(int swipeDetectionTimeoutInMs)
{
    this->swipeTimeout = swipeDetectionTimeoutInMs;
}

QGestureRecognizer::Result MouseSwipeRecognizer::recognize(QGesture* state, QObject* watched, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        // Activate swipe recognition
        this->activateSampling = true;
        this->swipeTimeoutTimer.start();

        return ResultFlag::MayBeGesture;
    }
    else if (event->type() == QEvent::MouseButtonRelease) {
        this->activateSampling = false;

        processData();

        if (this->swipeDetected) {
            if (this->swipeTimeoutTimer.hasExpired(this->swipeTimeout)) {
//                qDebug().noquote().nospace() << "Swipe detected but not within " << this->swipeTimeout << "ms";
                return ResultFlag::CancelGesture;
            }

            MouseSwipeGesture* swipe = static_cast<MouseSwipeGesture*>(state);
            if (swipe && this->swipeProgress.length() >= 2) {
//                swipe->setHotSpot(this->swipeProgress.first());   // QGestureManager fails to deliever event if we set the hot spot?!
//                swipe->setHotSpot(static_cast<QWidget*>(watched)->mapFromGlobal(this->swipeProgress.first().toPoint()));
                swipe->setStart(this->swipeProgress.first());
                swipe->setEnd(this->swipeProgress.last());
                swipe->setDirection(this->directionTemp);

//                qDebug().noquote().nospace() << "Swipe detected in " << this->swipeTimeoutTimer.elapsed() << "ms";
                return ResultFlag::FinishGesture;
            }
            else {
                return ResultFlag::Ignore;
            }
        }
        else {
            return ResultFlag::CancelGesture; // Will also invalidate the timer
        }

        this->swipeDetected = false;
    }
    else if (event->type() == QEvent::MouseMove && this->activateSampling) {
        QMouseEvent* mouseMove = static_cast<QMouseEvent*>(event);

        if (this->swipeTimeoutTimer.hasExpired(this->swipeTimeout)) {
//            qDebug().noquote().nospace() << "Swipe detected but not within " << this->swipeTimeout << "ms. Gesture will be cancelled";
            return ResultFlag::CancelGesture; // Will also invalidate the timer
        }

        if (mouseMove) {
            this->swipeProgress.append(mouseMove->localPos());
            return ResultFlag::MayBeGesture;
        }
        else {
            return ResultFlag::Ignore;
        }
    }
    else {
        return ResultFlag::Ignore;
    }
}

QGesture*MouseSwipeRecognizer::create(QObject* target)
{
    MouseSwipeGesture* swipe = new MouseSwipeGesture(target); // target as parent or nullptr?!?
    return static_cast<QGesture*>(swipe);
}

void MouseSwipeRecognizer::reset(QGesture* state)
{
    // Invalidate the timer automatically whenever CancelGesture or FinishGesture is returned by the recognition process
    this->swipeTimeoutTimer.invalidate();

    this->swipeProgress.clear();
    this->directionTemp = MouseSwipeGesture::SwipeDirection::Undefined;

    MouseSwipeGesture* swipe = static_cast<MouseSwipeGesture*>(state);
    if (swipe) {
        swipe->setStart(QPointF());
        swipe->setEnd(QPointF());
        swipe->setDirection();
    }
}

void MouseSwipeRecognizer::processData()
{
    if (this->swipeProgress.length() < 2) {
        this->swipeDetected = false;
        return;
    }

    QPointF start = this->swipeProgress.first();
    QPointF end = this->swipeProgress.last();

    int countPointsCloseToLine = 0;

    foreach (QPointF mousePos, this->swipeProgress) {
        if (this->swipeTimeoutTimer.hasExpired(this->swipeTimeout)) {
            qDebug() << "Swipe recognition process interrupted due to timeout. Gesture will be cancelled";
            this->swipeDetected = false;
            return;
        }
        // Calculate distance from current sample to the line drawn between the first and last sample. This check is also redundantly applied to the first and last sample but it's okay
        double distanceToLine = qFabs((end.y() - start.y())*mousePos.x() - (end.x() - start.x())*mousePos.y() + end.x()*start.y() - end.y()*start.x())/qSqrt(qPow((end.y() - start.y()), 2) + qPow((end.x() - start.x()), 2));

        if (distanceToLine <= 30.0) {
            countPointsCloseToLine++;
        }
    }

    // Just as an experiment - swipe is detected if at least 2/3 of all samples are at a distance equal or smaller than 30 from the line drawn between the first and last sample
    this->swipeDetected = (countPointsCloseToLine >= this->swipeProgress.length()*2/3);

    if (!this->swipeDetected) {
        return;
    }

    // Now the direction needs to be calculated
    // Create a vector between the end and start point
    QVector2D swipeVector(QVector2D(end) - QVector2D(start));
    swipeVector.normalize();
    QVector2D base(1.0, 0.0);
    // and use it to calculate the angle relative to base (the X axis of the widget)
    double cosSwipeBase = (QVector2D::dotProduct(base, swipeVector))/(base.length()*swipeVector.length());
    double angleSwipe = qRadiansToDegrees(qAcos(cosSwipeBase));
    qDebug() << "angle: " << angleSwipe;

    // Create the 16 regions
    float intervalStep = 360.0/16.0;

    // Determine category of the movement
    // Horizontal swipe
    if ((angleSwipe >= 360.0 - intervalStep && angleSwipe <= 360.0) || (angleSwipe >= 0.0 && angleSwipe < 0.0 + intervalStep) || (angleSwipe >= 180.0 - intervalStep && angleSwipe < 180.0 + intervalStep)) {
        // Determine the actual direction of the horizontal swipe based on the X coordinate of the starting and end point
        // Smaller X means it's closer to the left side of the widget and vice versa
        if (end.x() > start.x()) {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::LeftToRight;
        }
        else {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::RightToLeft;
        }
    }
    // Since (0,0) is in the top left corner Y's value grows when moving downwards!
    // Vertical swipe
    else if ((angleSwipe >= 90.0 - intervalStep && angleSwipe < 90.0 + intervalStep) || (angleSwipe >= 270.0 - intervalStep && angleSwipe < 270.0 + intervalStep)) {
        // Determine the actual direction of the vertical swipe based on the Y coordinate of the starting and end point
        // Smaller Y means it's closer to the top side of the widget and vice versa
        if (end.y() > start.y()) {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::TopToBottom;
        }
        else {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::BottomToTop;
        }
    }
    // Diagonal swipe
    else if ((angleSwipe >= 270.0 + intervalStep && angleSwipe < 360.0 - intervalStep) || (angleSwipe >= 90.0 + intervalStep && angleSwipe < 180.0 - intervalStep)) {
        if (end.y() > start.y()) {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::TopRightToBottomLeft;
        }
        else {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::BottomRightToTopLeft;
        }
    }
    else if ((angleSwipe >= 0.0 + intervalStep && angleSwipe < 90.0 - intervalStep) || (angleSwipe >= 180.0 + intervalStep && angleSwipe < 270.0 - intervalStep)) {
        if (end.y() > start.y()) {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::TopLeftToBottomRight;
        }
        else {
            this->directionTemp = MouseSwipeGesture::SwipeDirection::BottomLeftToTopRight;
        }
    }
    else {
        // Shouldn't occur but still we want to give some feedback if it does occur. Again the algorithm is not perfect so who knows
        qDebug() << "Something went wrong with the swipe...";
    }
}

