#include "customgestures.h"
#include <QTouchEvent>
#include <QtMath>

#include <QtDebug>
#include <QFile>

#include <iostream>
using std::cout;
using std::endl;

bool LRecognizer::lineApproximation()
{
    if(positions.size() < 2) {
        positions.clear();
        return false;
    }

    cout << "Processing gathered positions" << endl;
    // Least square method for line approximation
    // Calculate average X and Y
    double xAvg = 0;
    double yAvg = 0;
    cout << "Touch points: ";
    foreach (QPointF p, positions) {
        cout << p.x() << "," << p.y() << "\n";
        xAvg += p.x();
        yAvg += p.y();
    }
    cout << endl;
    xAvg /= positions.size();
    yAvg /= positions.size();

    // Calculate slope
    double slope = 0;
    foreach (QPointF p, positions)
        slope += (p.x() - xAvg)*(p.y() - yAvg)/qPow((p.x() - xAvg), 2);

    // Send line equation parameters to UI
    double yInterception = yAvg - slope*xAvg;

    pStart.setX(0);
    pStart.setY(slope*pStart.x() + yInterception);
    pEnd.setX(100);
    pEnd.setY(slope*pEnd.y() + yInterception);

//    pDist = pEnd - pStart;
//    pNorm = pDist/(qSqrt(qPow(pDist.x(), 2) + qPow(pDist.y(), 2)));
//    // The end point will be at a distance 30 from the starting point
//    double distance = 10;
//    Q_ASSERT(distance!=0);
//    pEnd.setX(pStart.x() + pNorm.x()*distance);
//    pEnd.setY(pStart.y() + pNorm.y()*distance);

//    cout << "====================================\n"
//         << "Average point:\n\tx = " << xAvg << "\n\ty = " << yAvg << "\n"
//         << "Slope = " << slope << "\n"
//         << "Y-interception = " << yInterception << "\n"
//         << "START : x=" << pStart.x() << " | y=" << pStart.y() << "\n"
//         << "END : x=" << pEnd.x() << " | y=" << pEnd.y() << "\n"
//         << "===================================="
//         << endl;

//    qDebug("--------------------------------\nAverage point:\n\tx = %f\n\ty = %f\nSlope = %f\nY-interception = %f\nSTART : x=%f | y=%f\nEND : x=%f | y=%f\n--------------------------------",
//           xAvg, yAvg, slope, yInterception, pStart.x(), pStart.y(), pEnd.x(), pEnd.y());

    // Clear all position
    positions.clear();
    return true;
}

//QGesture *LRecognizer::create(QObject *target)
//{
//    return static_cast<QGesture *>(new LGesture());
//}

QGestureRecognizer::Result LRecognizer::recognize(QGesture *state, QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::TouchUpdate) {
        QTouchEvent *touch = static_cast<QTouchEvent *>(event);
        if (touch->touchPoints().size() == 1) {
            QTouchEvent::TouchPoint touchPoint = touch->touchPoints().at(0);
            cout << "touchPoint->pos() = [x: " << touchPoint.pos().x() << " | y: " << touchPoint.pos().y() << "]" << endl;
            // If no other touch event is detected the standard procedure is to gather 30 (can be adjusted) samples
            // before running the linear regression
            if (positions.size() < 30) {
                QPointF tp = touchPoint.pos();
                // Scale down the points' coordinates since we work with pixel coordinates which creates very large values during the approximation
                tp.setX(tp.x()/10);
                tp.setY(tp.y()/10);
                positions.append(tp);
                return MayBeGesture;
            } else {
                if(lineApproximation()) {
                    state->setProperty("name", QLatin1String("Detected LINE gesture"));
                    state->setProperty("pStart", pStart);
                    state->setProperty("pEnd", pEnd);
                    return FinishGesture;
                }
                else return CancelGesture;
            }
        }
        else  {
            // A different from a single touch event is received attempt to detect the line with the current number of points
            if(lineApproximation()) {
                state->setProperty("name", QLatin1String("Detected LINE gesture (prematurely)"));
                state->setProperty("pStart", pStart);
                state->setProperty("pEnd", pEnd);
                return FinishGesture;
            }
            else return CancelGesture;
        }
    }

    return Ignore;
}

LRecognizer::LRecognizer()
    : QGestureRecognizer()
{

}

//LGesture::LGesture(QObject *parent)
//    : QGesture(parent),
//      name("L_GESTURE")
//{

//}

//QString LGesture::getName()
//{
//    return name;
//}

//void LGesture::setName(QString _name)
//{
//    name = _name;
//}

//QPointF LGesture::getPStart()
//{
//    return pStart;
//}

//void LGesture::setPStart(QPointF _pStart)
//{
//    pStart = _pStart;
//}

//QPointF LGesture::getPEnd()
//{
//    return pEnd;
//}

//void LGesture::setPEnd(QPointF _pEnd)
//{
//    pEnd = _pEnd;
//}



/****************************************************************************************/

FFTRecognizer::FFTRecognizer()
    : QGestureRecognizer()
{
}

//QGesture *FFTRecognizer::create(QObject *target)
//{
//    return static_cast<QGesture *>(new FFTGesture());
//}

QGestureRecognizer::Result FFTRecognizer::recognize(QGesture *state, QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::TouchUpdate) {
        if(watched) {
            cout << "Watched object for recognizer is " << watched->objectName().toStdString() << endl;
        }
        QTouchEvent *touch = static_cast<QTouchEvent *>(event);
        if (touch->touchPoints().size() == 4) {
            // Set name property for the FourFingerTap gesture to "FFT_GESTURE"
            state->setProperty("name", QLatin1String("Detected four finger tap gesture"));
            return FinishGesture;
        }
    }

    return Ignore;
}

//FFTGesture::FFTGesture(QObject *parent)
//    : QGesture(parent),
//      name("FFT_GESTURE")
//{

//}

//QString FFTGesture::getName()
//{
//    return name;
//}

//void FFTGesture::setName(QString _name)
//{
//    name = _name;
//}
