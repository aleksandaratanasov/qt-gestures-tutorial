#ifndef GESTURESCENE_HPP
#define GESTURESCENE_HPP

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include "mousegestures.h"

class GestureView : public QGraphicsView
{
public:
    GestureView(QGraphicsScene *scene, QWidget *parent = Q_NULLPTR);

    Qt::GestureType getMsType();
    Qt::GestureType getMpType();
private:
    MouseSwipeRecognizer* msRecognizer;
    MousePinchRecognizer* mpRecognizer;

    Qt::GestureType mouseSwipeType;
    Qt::GestureType mousePinchType;

    bool event(QEvent *event) override;
    bool gestureEvent(QGestureEvent *event);

    QGraphicsRectItem* rect;
};

#endif // GESTURESCENE_HPP
