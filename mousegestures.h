#ifndef MOUSEGESTURES_H
#define MOUSEGESTURES_H

#include <QGesture>
#include <QGestureEvent>
#include <QGestureRecognizer>

#include <QElapsedTimer>

#include <QList>
#include <QPointF>
#include <QVector2D>
#include <QObject>

/**
 * @brief The MousePinchGesture class reprents a pinch gesture using
 * the mouse. It has two directions - inward and outward.
 *
 *  See the {@link MousePinchRecognizer} for information on how such
 * a gesture is generated
 */
class MousePinchGesture : public QGesture
{
    Q_OBJECT

    Q_PROPERTY(QPointF firstLeft
               READ getFirstLeft
               WRITE setFirstLeft)

    Q_PROPERTY(QPointF secondRight
               READ getSecondRight
               WRITE setSecondRight)

    Q_PROPERTY(double stretch
               READ getStretch
               WRITE setStretch)

    Q_PROPERTY(PinchDirection direction
               READ getDirection
               WRITE setDirection)

    Q_PROPERTY(double scale
               READ getScale
               WRITE setScale)

    Q_PROPERTY(double angle
               READ getAngle
               WRITE setAngle)

  public:
    /** Determines whether the pinch goes in- or outwards based on the factor variable */
    enum PinchDirection {
      Undefined = 0,
      Inward = 1,
      Outward = 2,
      Unchanged = 3
    };
    Q_ENUM(PinchDirection)

    explicit MousePinchGesture(QObject* parent = Q_NULLPTR);

    QPointF getFirstLeft() const;
    void setFirstLeft(const QPointF& firstPinchPoint);

    QPointF getSecondRight() const;
    void setSecondRight(const QPointF& secondPinchPoint);

    double getStretch() const;
    void setStretch(double stretch);

    PinchDirection getDirection() const;
    void setDirection(PinchDirection direction = MousePinchGesture::PinchDirection::Undefined);

    double getScale() const;
    void setScale(double scale);

    double getAngle() const;
    void setAngle(double angle);

  private:
    /** The first pinch location of the mouse when the left mouse button has been pressed */
    QPointF firstLeft;
    /** The second pinch location of the mouse when the right mouse button has been pressed */
    QPointF secondRight;
    /** Distance between the two pinch points. Used as an acceleration component during the increasing/decreasing of the pinch factor */
    double stretch;
    /** Stores the direction (see {@link PinchDirection}) in which the pinch is "dragged". If the factor increases {@link Outward} is used else {@link Inward} */
    PinchDirection direction;
    /** Scale factor affected by the acceleration which is stored inside stretch. Scaling is orientation invariant */
    double scale;
    /**
     * @brief The rotation angle which is created between the X axis of the coordinate system (represented by the direction dector between
     * the {@link firstLeft} and {@link secondRight} pinch points; the {@link firstLeft} is the origin of the coordinate system) and the
     * vector from the origin to the current position of the mouse while the right mouse button is pressed
     */
    double angle;
};

class MousePinchRecognizer : public QGestureRecognizer
{
  public:
    MousePinchRecognizer();

    QGestureRecognizer::Result recognize(QGesture* state, QObject* watched, QEvent* event) override;

    /**
     * @brief Creates and returns an instance of {@link MousePinchGesture} (downcasted to {@link QGesture})
     * @param target The receiver of the gesture. The created gesture's parent is set to this value
     * @return An instance of a gesture that will be populated with data throughout the recognition process and can
     * later on be retrieved by capturing the registered gesture event for the mouse pinch recognizer
     */
    QGesture* create(QObject* target) override;

    /**
     * @brief Called whenever a gesture has been marked as cancelled. It cleans up the current recognition artifacts
     * (like the timer and the list of samples) in order to prepare the recognizer for the next attempt for a swipe gesture
     * @param state The cancelled gesture
     */
    void reset(QGesture* state) override;
  protected:
    /**
     * @brief The actual pinch recognition process takes place here. In order to trigger a pinch gesture following
     * sequence of actions needs to take place:
     *  - first the left mouse button is pressed and released. This triggers the saving of the {@link firstLeft} pinch point
     *  - next the mouse is moved at some distance from the original point (if not, no pinch will be triggered)
     *  - clicking the right mouse button stores the {@link secondRight} pinch point
     *  - while holing the right mouse button drag the mouse in some direction
     *
     * There are multiple pieces of data that are processed and stored:
     *  - the {@link firstLeft} pinch point sets the origin of the coordinate system used to determine the rotation angle
     *  - the distance between the {@link firstLeft} and the very first {@link secondRight} pinch points is used
     *    to determine the stretch factor which plays the role of an acceleration factor when dragging the mouse
     *    during the pinch. The larger the stretch factor is, the faster the scaling will be
     *  - a direction vector from {@link firstLeft} to {@link secondRight} determines the X axis of the coordinate system starting
     *    from the origin.
     *  - while dragging the angle and distance between the origin and the current mouse position are caluclated (using
     *    the value of {@link secondRight}). The angle is then used to set the rotation angle of the component and the distance
     *    is combined with the stretch factor to generate the scaling factor
     */
    virtual void processData();
  private:
    enum PinchState {
      LeftButtonPressed = 0,
      LeftButtonReleased = 1,
      RightButtonPressed = 2
    };

    /**
     * @brief Stores the states of the pinch gesture:
     *  - first cell stores the {@link LeftButtonPressedState} state
     *  - second cell stores the {@link LeftButtonReleased} state
     *  - third cell stores the {@link RightButtonPressed} state
     * Whenver the left mouse button is pressed {@link LeftButtonPressedState}
     * is toggled to true. Upon releasing the left mouse button
     * {@link LeftButtonPressedState} is set to false and {@link LeftButtonReleased}
     * is set to true. Last the right mouse button is pressed and
     * {@link LeftButtonPressedState} is toggled to false but
     * {@link RightButtonPressed} is set to true
     *
     * While the third state is actived we have a confirmed pinch
     * gesture which gets updated until {@link RightButtonPressed} is set
     * to false again
     *
     */
    bool pinchStates[3];

    /**
     * @brief A direction vector from {@link secondRightInitial} to the origin (set to {@link firstLeft}).
     * It is used both to calculate {@link stretch}, which is equal to the length of the vector, as well as
     * to determine the angle created between it and the vector between the {@link secondRight} and the origin
     * which stores the current mouse position while pinching is active. The vector is at the core of computing
     * both the scale and the rotation angle
     */
    QVector2D xAxis;
    /**
     * @brief The vector from the current {@link secondRight} to the origin. Calculating its length (combined with
     * the stretch factor from {@link xAxis}) results in the scale factor while the angle between it and the {@link xAxis}
     * represents the rotation angle
     */
    QVector2D currentSecondRightToOrigin;
    QVector2D previousSecondRightToOrigin;

    /** First pinch point stored by pressing and releasing the left mouse button */
    QPointF firstLeft;

    /** Second pinch point activated by the movement of the mouse while pressing the right mouse button */
    QPointF secondRight;

    /** Contains the direction (by default {@link PinchDirection::Undefined}) of the pinch gesture */
    MousePinchGesture::PinchDirection directionTemp;
    /**
     * @brief Contains the distance between the origin of the coordinate system
     * (set to {@link firstLeft}) and the initial point set to {@link secondRightInitial}.
     * It is equal to the length of {@link xAxis}
     */
    double stretch;
    double scaleTemp;
    double angleTemp;
};

/**
 * @brief The MouseSwipeGesture class represents a swipe (flick) gesture using
 * the mouse. It supports 8 movements split into 3 major categories:
 *  - horizontal - this category is split into 2 types each representing a horizontal movement:
 *    - left to right
 *    - right to left
 *  - vertical - also contains 2 types but for vertical movements:
 *    - top to bottom
 *    - bottom to top
 *  - diagonal - supports 4 types of diagonal movements:
 *    - top left to bottom right
 *    - top right to bottom left
 *    - bottom left to top right
 *    - bottom right to top left
 * See the {@link MouseSwipeRecognizer} for information on how such
 * a gesture is generated
 */
class MouseSwipeGesture : public QGesture
{
    Q_OBJECT

    Q_PROPERTY(QPointF start
               READ getStart
               WRITE setStart)

    Q_PROPERTY(QPointF end
               READ getEnd
               WRITE setEnd)

    Q_PROPERTY(SwipeDirection direction
               READ getDirection
               WRITE setDirection)
public:
    /** Swipe direction */
    enum SwipeDirection {
        Undefined = 0,  /** Default value. Does not represent a movement per se */
        // Horizontal
        LeftToRight = 1,
        RightToLeft = 2,
        // Vertical
        TopToBottom = 3,
        BottomToTop = 4,
        // Diagonal
        TopLeftToBottomRight = 5,
        TopRightToBottomLeft = 6,
        BottomLeftToTopRight = 7,
        BottomRightToTopLeft = 8
    };
    Q_ENUM(SwipeDirection)

    /**
     * @brief Constructor for the mouse swipe gesture. It is managed by the Qt gesture manager and should not be created manually
     * @param parent Parent object set by the Qt gesture manager
     */
    MouseSwipeGesture(QObject* parent = Q_NULLPTR);

    /** @brief Returns the starting point of the swipe gesture */
    QPointF getStart() const;

    /**
     * @brief Sets the startring point of the swipe gesture
     * @param start Starting point of the swipe gesture
     */
    void setStart(const QPointF& start);

    /** @brief Returns the end point of the swipe gesture */
    QPointF getEnd() const;

    /**
     * @brief Sets the end point of the swipe gesture
     * @param end End point of the swipe gesture
     */
    void setEnd(const QPointF& end);

    /** @brief Returns the direction of the swipe gesture */
    SwipeDirection getDirection() const;

    /**
     * @brief Sets the direction of the swipe
     * @param direction By default set to {@link SwipeDirection::Undefined} (also for when gesture is reset) contains the direction of
     *                  the swipe gesture
     */
    void setDirection(SwipeDirection direction = MouseSwipeGesture::SwipeDirection::Undefined);
private:
    /** Contains the direction (by default {@link SwipeDirection::Undefined}) of the swipe gesture */
    SwipeDirection direction;

    /** Starting point of the swipe gesture */
    QPointF start;

    /** End point of the swipe gesture */
    QPointF end;
};

/**
 * @brief The MouseSwipeRecognizer class represents a custom gesture recognizer for detecting swipe movements with the mouse
 * The algorithm is very simplistic but it is also meant for demonstration purposes. For more information see {@link MouseSwipeRecognizer::recognize recognize}
 * Just like other recognizers once registered the ownership is transferred to the application and in terms of memory management
 * should not be manually released unless unregistered first. In order to use the recognizer do the following:
 *  - dynamically instantiate the recognizer
 *  - using {@link QGestureRecognizer::registerRecognizer() registerRecognizer()} register it
 *  - store the returned automatically generated gesture type for later use
 *  - activate the recognizer by passing the above generated gesture type to {@link QWidget::grabGesture() grabGesture()}
 *  - override {@link QWidget::event() event()} of your widget that needs to grab the gesture
 *  - inside the event handler check for received events of type {@link QEvent::Gesture}
 *  - if gesture event captured cast the event to {@link QGestureEvent}
 *  - retrieved the contained gesture from the event by using {@link QGestureEvent::gesture()} and passing
 * the above generated gesture type to retrieve exactly the mouse swipe gesture
 *  - cast the retrieved generic {@link QGesture} to {@link MouseSwipeGesture}, accept the event and alter the UI as you see fit
 *
 * Note that the recognition process has a timeout. Even if an accurate swipe movement is made if not done within the given time limit
 * it will be cancelled
 */
class MouseSwipeRecognizer : public QGestureRecognizer
{
public:
    /**
     * @brief Constructor for the recognizer
     * @param swipeDetectionTimeoutInMs Sets (default: 350ms) the time limit for the recognition process
     */
    MouseSwipeRecognizer(int swipeDetectionTimeoutInMs = 350);

    void setDetectionTimeout(int swipeDetectionTimeoutInMs = 350);

    /**
     * @brief The recognition takes place here (for more information see {@link processData()} which)
     * is called here internally once enough samples have been gathered).
     *
     * Once the user presses a mouse button the timer (responsible for the time limit of the recognition process)
     * is started and the sampling is activated by setting {@link activateSampling} to true. This state of the
     * recognizer is also referred to as sampling activation state
     *
     * While holding the mouse button the user needs to move the mouse within the set time limit in one of the
     * 8 directions defined by {@link MouseSwipeGesture::SwipeDirection}. The straighter the trajecotry, the liklier
     * it is for the recognizer to detect an actual swipe gesture. This state of the recognizer is also referred to
     * as sampling state
     *
     * The actual recognition process is triggered once the mouse button has been released. Note that the time limit
     * defines the full recognition process from gather sampling to processing each sample and determining whether
     * the movement was a swipe movement or not. If a mouse swipe gesture is detected within the given time limit
     * the recognizer passes the gesture through a {@link QGestureEvent} onto the widget that needs to process the gesture
     * on the UI level. This final state of the recognizer is also referred to as the gesture recognition state
     *
     * @param state Contains the current state of the mouse swipe gesture (as created by {@link create()})
     * @param watched The received of the gesture. This is the widget that where the registration of the recognizer and the grabbing of the gesture
     *                has taken place
     * @param event Some event. If the event is a mouse button press/release or mouse move event it will cause the recognizer to take action and
     *              change its recognition state to a new one (see above). All other events will be ignored
     * @return Returns the result of the recognizer for the given state
     */
    QGestureRecognizer::Result recognize(QGesture* state, QObject* watched, QEvent* event) override;

    /**
     * @brief Creates and returns an instance of {@link MouseSwipeGesture} (downcasted to {@link QGesture})
     * @param target The receiver of the gesture. The created gesture's parent is set to this value
     * @return An instance of a gesture that will be populated with data throughout the recognition process and can
     * later on be retrieved by capturing the registered gesture event for the mouse swipe recognizer
     */
    QGesture* create(QObject* target) override;

    /**
     * @brief Called whenever a gesture has been marked as cancelled. It cleans up the current recognition artifacts
     * (like the timer and the list of samples) in order to prepare the recognizer for the next attempt for a swipe gesture
     * @param state The cancelled gesture
     */
    void reset(QGesture* state) override;
protected:
    /**
     * @brief The actual swipe recognition process takes place here and based on the outcome (including an expired timer)
     * is stored in {@link swipeDetected} which is then processed by the {@link recognize()} method.
     *
     * The algorithm is very crude and is for demonstration purposes only (although it seems to work quite well). A swipe
     * gesture is defined as a movement (in one of the 8 possible direction as defined by {@link MouseSwipeGesture::SwipeDirection})
     * in a more or less straight line. Due to the low probability of a user being able to "draw" a perfect straight line
     * we give some tolerance to how the points are distributed.
     *
     * The samples contain three major parts - the first (starting point of the gesture), the last (the end point of the gesture)
     * and all the points in-between.
     *
     * First step is to connect the first and last point of the sampling list in order to form a virtually perfect straight swipe path
     * This will be used as a reference for all the samples in between.
     *
     * The next step is to calculate the distance to this line for each sample. After some observations a constant value was used but
     * it may be changed in the code (or outside the method by adding the proper setter). The trajectory which the samples represent
     * is considered to be a successful swipe in case 2/3 of all samples are at an equal or lesser distance to the virtually perfect
     * straight swipe path based on that constant value.
     *
     * One the algorithm has determined that a swipe movement has taken place as a last step we need to determine the direction
     * of the movement. Here we use very simple geometry by calculating the angle between the virtually perfect straight swipe path
     * and the X axis of the widget. Based on the angle we determine where in the 4 quadrants the line actually is. 4 quadrants are
     * however not enough for 8 direction. We subdivide the X and Y coordinate system into 16 equal pieces thus creating an step of
     * 1/16th of 360 degrees. A pair of two opposite to each other pieces is responsible for handling two gestures of the same type
     * (horizontal, vertical or diagonal) but in opposite directions. This subdivision is responbile for determining the category but
     * not the exact direction. In order to do that the X and Y coordinates of the starting and end points are taken into account.
     *
     * *Example:*
     * A **top to bottom** swipe gesture is a **vertical** gesture. As such it falls into the regions between 90 - (1/16 of 360) degrees
     * and 90 + (1/16 of 36 degrees) or 180 - (1/16 of 360 degrees) and 180 + (1/16 of 360 degrees). Due to the fact that the movement
     * starts from the top and moves to the bottom of the widget the Y coordinate at the start is smaller than the Y coordinate at the end.
     * (note: that local coordinate system which contains the widget starts at (0,0) at the top left corner!) Based on this difference
     * we can say for sure that the starting point lies above the end point of the swipe path hence finalizing the process of determing
     * the direction of the swipe gesture.
     *
     * All the data generated in this method (direction, starting and end point) are used inside {@link recognize()} to populate
     * the gesture. The algorithm above has proven to be also quite effective when handling slightly curved swipe gestures which means
     * that the user can do faster movement with the mouse (in order to create a swipe gesture) without paying too much attention of
     * how accurate the created path is
     */
    virtual void processData();
private:
    QElapsedTimer swipeTimeoutTimer;
    /**
     * @brief Contains the time limit for the recognition process. Note that limiting the time the user has to
     * make a swipe movement with the mouse may result in unusable recognizer. Use {@link setDetectionTimeout} to
     * change the time limit if this happens.
     *
     * The time limit cancels a gesture even if the gesture is a valid one. Choosing the proper limit will improve
     * the detection ration since the user will be forced to make more accurate movements to keep up with time
     */
    int swipeTimeout;

    /**
     * @brief Contains the result of the recognition (set inside {@link processData()}). If true a gesture was mouse swipe gesture
     * has been successfully detected within the given time limit.
     */
    bool swipeDetected;

    /** Based on its value the recognizer starts/stops gathering mouse positions as samples for the recognition process later on */
    bool activateSampling;

    /**
     * It is regularly updated during the recognition process to contain the latest mouse position (along with all before)
     * The recognition evaluates constantly if a swipe can be extracted from the list of points. Whenever the timeout kicks in or
     * the gesture is cancelled/completed, the list is emptied so that the next attempt for recognition can start
     *
     * The first sample is the starting point while the last is the end point of the swipe gesture
     */
    QList<QPointF> swipeProgress;

    /** A temporary holder of the direction of the swipe gesture */
    MouseSwipeGesture::SwipeDirection directionTemp;
};

#endif // MOUSEGESTURES_H
