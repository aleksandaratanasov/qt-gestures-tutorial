#include <QRectF>
#include <QtMath>
#include <QList>
#include <QDebug>
#include "gestureview.h"

GestureView::GestureView(QGraphicsScene* scene, QWidget* parent)
    : QGraphicsView(scene, parent)
{
    setSceneRect(this->scene()->sceneRect());

    this->msRecognizer = new MouseSwipeRecognizer(300);
    this->mouseSwipeType = QGestureRecognizer::registerRecognizer(this->msRecognizer);
    grabGesture(this->mouseSwipeType);

    this->mpRecognizer = new MousePinchRecognizer();
    this->mousePinchType = QGestureRecognizer::registerRecognizer(this->mpRecognizer);
    grabGesture(this->mousePinchType);

    this->rect = new QGraphicsRectItem();
    this->rect->setRect(QRectF(0, 0, 50, 100));
    this->scene()->addItem(static_cast<QGraphicsItem*>(this->rect));
}

Qt::GestureType GestureView::getMsType()
{
    return this->mouseSwipeType;
}

Qt::GestureType GestureView::getMpType()
{
    return this->mousePinchType;
}

bool GestureView::event(QEvent* event)
{
    if(event->type() == QEvent::Gesture) {
        return gestureEvent(static_cast<QGestureEvent*>(event));
    }
    return QGraphicsView::event(event);
}

bool GestureView::gestureEvent(QGestureEvent* event)
{
    if (!this->rect) {
        return false;
    }

    if (QGesture* g = event->gesture(this->mousePinchType)) {
        MousePinchGesture* mp = static_cast<MousePinchGesture*>(g);
        if (mp) {
            qDebug() << "pinch";

            qDebug() << "(stretch: " + QString::number(mp->property("stretch").value<double>()) + ")";

            double angle = mp->property("angle").value<double>();
            qDebug() << "(angle: " + QString::number(angle) + ")";
            this->rect->setRotation(angle);

            double scale = mp->property("scale").value<double>();
            qDebug() << "(scale: " + QString::number(scale) + ")";
            QRectF scaledRect = this->rect->rect();
            switch(int(mp->property("direction").value<MousePinchGesture::PinchDirection>())) {
              case MousePinchGesture::PinchDirection::Inward:
                qDebug() << " (inward)";
                scaledRect.setWidth(qFabs(scaledRect.width() - scale));
                scaledRect.setHeight(qFabs(scaledRect.height() - scale));
                break;
              case MousePinchGesture::PinchDirection::Outward:
                qDebug() << " (outward)";
                scaledRect.setWidth(scaledRect.width()+ scale);
                scaledRect.setHeight(scaledRect.height() + scale);
                break;
              case MousePinchGesture::PinchDirection::Unchanged:
                qDebug() << " (unchanged)";
                break;
              default:
                qDebug() << "---";
            }
            // FIXME It seems that updates for the rectangle are triggered however these are reset to a very big size
            this->rect->setRect(scaledRect);
            updateScene(QList<QRectF>() << this->sceneRect());
            qDebug() << "newRect: " << scaledRect.width() << "x" << scaledRect.height();

            event->accept(mp);
        }
        else {
            event->ignore(mp);
        }
    }
    return QGraphicsView::event(event);
}
