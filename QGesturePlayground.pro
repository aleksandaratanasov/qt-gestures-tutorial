#-------------------------------------------------
#
# GESTURES PLAYGROUND WITH QT
# Project created by QtCreator 2016-07-18T16:05:14
# Author: Aleksandar Atanasov
# Description: This project demonstrates the Qt Gesture API by implementing custom gesture recognition and binding it to
# a custom QWidget
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QGesturePlayground
TEMPLATE = app

SOURCES  += main.cpp\
    gesturewidget.cpp \
    gesturelabel.cpp \
    customgestures.cpp \
    mousegestures.cpp \
    gestureview.cpp

HEADERS  += gesturewidget.h \
    gesturelabel.h \
    customgestures.h \
    mousegestures.h \
    gestureview.h

FORMS    +=
