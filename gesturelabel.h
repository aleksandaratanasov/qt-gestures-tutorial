#ifndef GESTURELABEL_H
#define GESTURELABEL_H

#include <QtWidgets>
#include <QPaintEvent>
#include <QTimer>
#include "customgestures.h"
#include "mousegestures.h"

class GestureLabel : public QLabel
{
    Q_OBJECT
public:
    explicit GestureLabel(QWidget *parent = 0);
    ~GestureLabel();

    Qt::GestureType getLType();
    Qt::GestureType getFftType();
    Qt::GestureType getMsType();
    Qt::GestureType getMpType();
signals:
public slots:
private slots:
    void slotResetLabelTextUponTimeout();
protected:
    void paintEvent(QPaintEvent* event) override;
    void resizeEvent(QResizeEvent *event) override;
private:
    QTimer resetLabelTextTimer;

    LRecognizer* lRecognizer;
    FFTRecognizer* fftRecognizer;
    MouseSwipeRecognizer* msRecognizer;
    MousePinchRecognizer* mpRecognizer;

    Qt::GestureType lineType;
    Qt::GestureType fourFingerTapType;
    Qt::GestureType mouseSwipeType;
    Qt::GestureType mousePinchType;

    bool event(QEvent *event) override;
    bool gestureEvent(QGestureEvent *event);
};

#endif // GESTURELABEL_H
