#include <QDebug>
#include <QPainter>
#include <QFont>
#include <QFontMetrics>
#include <QSize>
#include "gesturelabel.h"

GestureLabel::GestureLabel(QWidget *parent) : QLabel(parent)
{
    setWordWrap(true);
//    setAttribute(Qt::WA_AcceptTouchEvents);
    this->lRecognizer = new LRecognizer();
    this->lineType = QGestureRecognizer::registerRecognizer(this->lRecognizer);
    grabGesture(this->lineType);

    fftRecognizer = new FFTRecognizer();
    this->fourFingerTapType = QGestureRecognizer::registerRecognizer(this->fftRecognizer);
    grabGesture(this->fourFingerTapType);

    this->msRecognizer = new MouseSwipeRecognizer(300);
    this->mouseSwipeType = QGestureRecognizer::registerRecognizer(this->msRecognizer);
    grabGesture(this->mouseSwipeType);

    this->mpRecognizer = new MousePinchRecognizer();
    this->mousePinchType = QGestureRecognizer::registerRecognizer(this->mpRecognizer);
    grabGesture(this->mousePinchType);

    this->resetLabelTextTimer.setInterval(1500);
    connect(&this->resetLabelTextTimer, SIGNAL(timeout()), this, SLOT(slotResetLabelTextUponTimeout()));
}

GestureLabel::~GestureLabel()
{
    // DO NOT delete the recognizers. These are managed by the application internally!
}

Qt::GestureType GestureLabel::getLType()
{
    return this->lineType;
}

Qt::GestureType GestureLabel::getFftType()
{
    return this->fourFingerTapType;
}

Qt::GestureType GestureLabel::getMsType()
{
    return this->mouseSwipeType;
}

Qt::GestureType GestureLabel::getMpType()
{
    return this->mousePinchType;
}

void GestureLabel::slotResetLabelTextUponTimeout()
{
    setText("");
    this->resetLabelTextTimer.stop();
}

void GestureLabel::paintEvent(QPaintEvent* event)
{
    QLabel::paintEvent(event);
}

void GestureLabel::resizeEvent(QResizeEvent* event)
{
//    QFont f = font();
//    QFontMetrics metrics(f);
//    QSize size = metrics.size(0, text());
//    float widthFactor = width() / (float)size.width();
//    float heightFactor = height() / (float)size.height();

//    float factor = qMin(widthFactor, heightFactor);
//    f.setPointSizeF(f.pointSizeF()*factor);
//    setFont(f);

    QLabel::resizeEvent(event);
}

bool GestureLabel::gestureEvent(QGestureEvent *event)
{
    QString gestureMsg = "";
    if(QGesture* g = event->gesture(this->fourFingerTapType)) {
        event->accept(g);
        gestureMsg = g->property("name").toString();
        this->resetLabelTextTimer.start();
    }
    else if (QGesture* g = event->gesture(this->lineType)) {
        event->accept(g);
        gestureMsg = g->property("name").toString();
        this->resetLabelTextTimer.start();
    }
    else if (QGesture* g = event->gesture(this->mousePinchType)) {
        MousePinchGesture* mp = static_cast<MousePinchGesture*>(g);
        if (mp) {
            event->accept(mp);
            gestureMsg = "pinch";

            gestureMsg += "(stretch: " + QString::number(mp->property("stretch").value<double>()) + ")";
            gestureMsg += "(scale: " + QString::number(mp->property("scale").value<double>()) + ")";
            gestureMsg += "(angle: " + QString::number(mp->property("angle").value<double>()) + ")";

            switch(int(mp->property("direction").value<MousePinchGesture::PinchDirection>())) {
              case MousePinchGesture::PinchDirection::Inward:
                gestureMsg += " (inward)";
                break;
              case MousePinchGesture::PinchDirection::Outward:
                gestureMsg += " (outward)";
                break;
              case MousePinchGesture::PinchDirection::Unchanged:
                gestureMsg += " (unchanged)";
                break;
              default:
                gestureMsg = "---";
            }

            this->resetLabelTextTimer.start();
        }
        else {
            event->ignore(mp);
        }
    }
    else if (QGesture* g = event->gesture(this->mouseSwipeType)) {
        MouseSwipeGesture* ms = static_cast<MouseSwipeGesture*>(g);
        if (ms) {
            event->accept(ms);
            gestureMsg = "swipe";
            switch (int(ms->property("direction").value<MouseSwipeGesture::SwipeDirection>())) {
                case MouseSwipeGesture::SwipeDirection::LeftToRight:
                    gestureMsg += " (left to right)";
                    break;
                case MouseSwipeGesture::SwipeDirection::RightToLeft:
                    gestureMsg += " (right to left)";
                    break;
                case MouseSwipeGesture::SwipeDirection::TopToBottom:
                    gestureMsg += " (top to bottom)";
                    break;
                case MouseSwipeGesture::SwipeDirection::BottomToTop:
                    gestureMsg += " (bottom to top)";
                    break;
                case MouseSwipeGesture::SwipeDirection::TopLeftToBottomRight:
                    gestureMsg += " (top left to bottom right)";
                    break;
                case MouseSwipeGesture::SwipeDirection::TopRightToBottomLeft:
                    gestureMsg += " (top right to bottom left)";
                    break;
                case MouseSwipeGesture::SwipeDirection::BottomLeftToTopRight:
                    gestureMsg += " (bottom left to top right)";
                    break;
                case MouseSwipeGesture::SwipeDirection::BottomRightToTopLeft:
                    gestureMsg += " (bottom right to top left)";
                    break;
                default:
                    gestureMsg = "---";
            }
            this->resetLabelTextTimer.start();
        }
        else {
            event->ignore(ms);
        }
    }

    setText(gestureMsg);
    return true;
}

bool GestureLabel::event(QEvent *event)
{
    if(event->type() == QEvent::Gesture) {
        return gestureEvent(static_cast<QGestureEvent*>(event));
    }
    return QWidget::event(event);
}
