#include <QDebug>
#include <QHBoxLayout>
#include <QGraphicsScene>

#include "gesturewidget.h"
#include "ui_gesturewidget.h"

GestureWidget::GestureWidget(QWidget *parent) :
    QWidget(parent)
{
    QHBoxLayout* layout = new QHBoxLayout(this);

    QGraphicsScene* scene = new QGraphicsScene(static_cast<QObject*>(this));
    scene->setSceneRect(QRectF(QPointF(0, 0), QSizeF(200, 200)));
    this->view = new GestureView(scene, this);
    layout->addWidget(this->view);

//    setAttribute(Qt::WA_AcceptTouchEvents);
    this->label = new GestureLabel(this);
    this->label->setObjectName("LABEL");
    this->label->setText("Waiting for a special gesture ;)");
    this->label->setAlignment(Qt::AlignCenter);
//    this->label->grabGesture(this->label->getFftType());
    this->label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout->addWidget(this->label);

    setLayout(layout);

    setGeometry(100, 100, 500, 500);
}

GestureWidget::~GestureWidget()
{
}


bool GestureWidget::event(QEvent *event)
{
    return QWidget::event(event);
}
